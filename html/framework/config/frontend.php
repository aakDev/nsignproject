<?php

$config = [
    'id' => 'web',
    'bootstrap' => ['robots'],
    'defaultRoute' => 'content/default/index',
    'on beforeRequest' => [\krok\catchAll\CatchAllHandler::class, 'handle'],
    'on afterRequest' => function () {
        /**
         * see. https://content-security-policy.com/
         */
        // Yii::$app->getResponse()->getHeaders()->add('Content-Security-Policy',
        //    'default-src \'none\'; script-src \'self\' \'unsafe-inline\'; connect-src \'self\'; child-src \'self\'; img-src * data:; style-src * \'unsafe-inline\'; font-src *;');
    },
    'container' => [
        'definitions' => [
            \krok\robots\RobotsInterface::class => [
                'class' => \krok\robots\Robots::class,
                'path' => '@runtime/robots/robots.txt',
                'lines' => [
                    'User-agent: *',
                ],
                'generators' => [
                    \krok\robots\generators\AllowGenerator::class,
                ],
            ],
            \anatoliy700\redirect\IRedirect::class => \anatoliy700\redirect\Redirect::class,
            \anatoliy700\redirect\repositories\IRepository::class => [
                'class' => \anatoliy700\redirect\repositories\csv\CSVRepository::class,
                'filePath' => '@app/redirectFile/redirect.csv',
            ],
            \anatoliy700\redirect\models\IRedirectItem::class => \anatoliy700\redirect\models\RedirectItem::class,
        ],
    ],
    'modules' => [
        'content' => [
            'viewPath' => '@krok/content/views/frontend',
            'controllerNamespace' => 'krok\content\controllers\frontend',
        ],
        'robots' => [
            'class' => 'anatoliy700\robots\Module',
        ],
        'catchAll' => [
            'class' => \yii\base\Module::class,
            'viewPath' => '@krok/catchAll/views/frontend',
            'controllerNamespace' => 'krok\catchAll\controllers\frontend',
        ],
        'redirect' => [
            'class' => \anatoliy700\redirect\Module::class,
            'layout' => '/index',
        ],
    ],
    'components' => [
        'urlManager' => [
            'class' => \yii\di\ServiceLocator::class,
            'components' => [
                'default' => require(__DIR__ . '/frontend/urlManager.php'),
                'backend' => require(__DIR__ . '/backend/urlManager.php'),
            ],
        ],
        'assetManager' => [
            'class' => \yii\web\AssetManager::class,
            'appendTimestamp' => true,
            'dirMode' => 0755,
            'fileMode' => 0644,
            'bundles' => [
                \yii\web\JqueryAsset::class => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js',
                    ],
                ],
                \yii\bootstrap\BootstrapAsset::class => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ],
                ],
                \yii\bootstrap\BootstrapPluginAsset::class => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ],
                ],
            ],
        ],
        'request' => [
            'class' => \krok\language\Request::class,
            'cookieValidationKey' => env('YII_COOKIE_VALIDATION_KEY'),
        ],
        'errorHandler' => [
            'class' => \yii\web\ErrorHandler::class,
            'errorAction' => 'redirect',
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => \yii\debug\Module::class,
        'panels' => [
            'user' => [
                'class' => \yii\debug\panels\UserPanel::class,
                'ruleUserSwitch' => [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
            'queue' => [
                'class' => \yii\queue\debug\Panel::class,
            ],
        ],
        'allowedIPs' => [
            '*',
        ],
    ];
}

return \yii\helpers\ArrayHelper::merge(require(__DIR__ . '/common.php'), $config);
